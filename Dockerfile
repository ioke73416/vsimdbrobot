FROM marketsquare/robotframework-browser

# Set Chrome to run in headless mode
ENV CHROME_OPTIONS="--headless --disable-gpu --no-sandbox"
USER root

RUN  apt-get update && \
     apt-get install -y python3-pip

USER pwuser
