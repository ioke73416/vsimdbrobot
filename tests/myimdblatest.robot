*** Settings ***
Library    OperatingSystem
Library    SeleniumLibrary    
Library    Collections
Library    String


*** Variables ***
${URL}           https://www.imdb.com/search/title/?num_votes=100,1000&genres=drama

${OutputFile}    output.txt



*** Test Cases ***


Scrape IMDb Web Page

    

    Create Webdriver  Chrome      
    Go To   ${URL}      

    # Clear the output file before every run
    Remove File    ${OutputFile}

    Set Selenium Implicit Wait    2 seconds 
    #click accept button
    ${accept_button}    Get WebElement  xpath://button[@class="icb-btn sc-bcXHqe sc-dkrFOg sc-iBYQkv dcvrLS ddtuHe dRCGjd" and @data-testid="accept-button"]
    ${accept_button_status}    Run Keyword And Return Status    Element Should Be Visible    ${accept_button}
    
     # Set implicit wait
    Set Selenium Implicit Wait    2 seconds    
    Click Element    ${accept_button}
    

    # Maximize Window
    Maximize Browser Window

    FOR    ${page_number}    IN RANGE    1    3   # Change the range according to the number of pages you want to scrape

        
       ${entries}    Get WebElements    xpath://div[@class="sc-5bc66c50-0 bZBaVw"] 
       ${titles}    Get WebElements    xpath://h3[@class="ipc-title__text"]
       ${ratings}    Get WebElements    xpath://span[@class="ipc-rating-star ipc-rating-star--base ipc-rating-star--imdb ratingGroup--imdb-rating"]

       

       ${title_list}    Create List
       ${ratings_list}     Create List
       ${metascores_list}     Create List
   
        Log To Console    Number of entries found: ${entries.__len__()}

        FOR    ${i}    IN RANGE    0    ${entries.__len__()}
           ${entry_text}    Get Text    ${entries[${i}]}
           Log To Console    Entry ${i}: ${entry_text}  # Log the entry text to see what is being captured
           ${title}    Get Text    ${titles[${i}]}
           ${rating}    Get Text   ${ratings[${i}]}
   
          # Extract metascore
           ${metascore}    Set Variable    ${entry_text.splitlines()[-2]}
          
           Log To Console    Title: ${title}    Rating: ${rating}    Metascore: ${metascore}  # Add this line to log the data collected

           Append To List    ${title_list}    ${title}
           Append To List    ${ratings_list}    ${rating}
           Append To List    ${metascores_list}    ${metascore}
           
           Append To File    ${OutputFile}    ${title};${rating};${metascore}\n
            
     
           
        END

   

        # Set implicit wait
        Set Selenium Implicit Wait    2 seconds

        ${next_link}    Get WebElement    xpath://span[@class="ipc-see-more__text"]
        
         # Set implicit wait
        Set Selenium Implicit Wait    1 seconds
        
        Scroll Element Into View    ${next_link}
        
        # Set implicit wait
        Set Selenium Implicit Wait    2 seconds

        
        Click Element    ${next_link}
        


       
 
    END
    
    Close Browser


    


