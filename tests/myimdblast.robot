*** Settings ***
Library    OperatingSystem
Library    SeleniumLibrary    
Library    Collections
Library    String


*** Variables ***
${URL}           https://www.imdb.com/search/title/?num_votes=100,1000&genres=drama

${OutputFile}    output.txt



*** Test Cases ***
Scrape IMDb Web Page

    Create Webdriver    Chrome       
    Go To   ${URL}      

    # Clear the output file before every run
    Remove File    ${OutputFile}

   

   
    # Set implicit wait
    Set Selenium Implicit Wait    2 seconds
    #click accept button
    ${accept_button}    Get WebElement  xpath://button[@class="icb-btn sc-bcXHqe sc-dkrFOg sc-iBYQkv dcvrLS ddtuHe dRCGjd" and @data-testid="accept-button"]
    ${accept_button_status}    Run Keyword And Return Status    Element Should Be Visible    ${accept_button}
    
        
    Click Element    ${accept_button}
    # Set implicit wait
    Set Selenium Implicit Wait    2 seconds

    # Maximize Window
    Maximize Browser Window

    FOR    ${page_number}    IN RANGE    1    3   # Change the range according to the number of pages you want to scrape

        
       ${entries}    Get WebElements    xpath://div[@class="sc-5bc66c50-0 bZBaVw"]
       ${titles}    Get WebElements    xpath://h3[@class="ipc-title__text"]
       ${ratings}    Get WebElements    xpath://span[@class="ipc-rating-star ipc-rating-star--base ipc-rating-star--imdb ratingGroup--imdb-rating"]
     
      

       ${title_list}    Create List
       ${ratings_list}     Create List
      

        FOR    ${i}    IN RANGE    0    ${entries.__len__()}
 

           ${title}    Get Text    ${titles[${i}]}
           ${rating}    Get Text   ${ratings[${i}]}
      



            Append To List    ${title_list}    ${title}
            Append To List    ${ratings_list}    ${rating}
          
            Append To File    ${OutputFile}    ${title};${rating}\n
            
            
            Log To Console    ${title}    # Print the movie title
            Log To Console    ${rating}     # Print the number of votes
          
            
           
        END

   

        
        # Log statement after the loop
        Log To Console  Titles extracted successfully

       ${next_link}    Get WebElement    xpath://button[@class="ipc-btn ipc-btn--single-padding ipc-btn--center-align-content ipc-btn--default-height ipc-btn--core-base ipc-btn--theme-base ipc-btn--on-accent2 ipc-text-button ipc-see-more__button"]
        
         # Set implicit wait
        Set Selenium Implicit Wait    1 seconds
        
        Scroll Element Into View    ${next_link}
        
        # Set implicit wait
        Set Selenium Implicit Wait    2 seconds

        
        Click Element    ${next_link}
        


       
 
    END
    
    Close Browser




    
