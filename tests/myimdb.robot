*** Settings ***
Library    OperatingSystem
Library    SeleniumLibrary    
Library    Collections
Library    String


*** Variables ***
${URL}           https://www.imdb.com/search/title/?num_votes=100,1000&genres=drama

${OutputFile}    output.txt
${MAX_PAGES}     3  # Adjust this number based on how many pages you want to scrape


*** Test Cases ***
Scrape IMDb Web Page

    Create Webdriver    Chrome       
    Go To   ${URL}      

    # Clear the output file before every run
    Remove File    ${OutputFile}


    # Set implicit wait
    Set Selenium Implicit Wait    2 seconds
    #click accept button
    ${accept_button}    Get WebElement  xpath://button[@class="icb-btn sc-bcXHqe sc-dkrFOg sc-iBYQkv dcvrLS ddtuHe dRCGjd" and @data-testid="accept-button"]
    ${accept_button_status}    Run Keyword And Return Status    Element Should Be Visible    ${accept_button}
    
        
    Click Element    ${accept_button}
    # Set implicit wait
    Set Selenium Implicit Wait    2 seconds

    # Maximize Window
    Maximize Browser Window
    
    # Set implicit wait
    Set Selenium Implicit Wait    2 seconds

     ${current_page}    Set Variable    1

    FOR    ${index}    IN RANGE   ${MAX_PAGES}     # Change the range according to the number of pages you want to scrape

       ${entries}    Get WebElements    xpath://div[@class="sc-b189961a-0 hBZnfJ"]
       ${ratings}    Get WebElements    xpath://span[@class="ipc-rating-star ipc-rating-star--base ipc-rating-star--imdb ratingGroup--imdb-rating"]

       FOR     ${entry}  IN    @{entries}
       ${entry_text}    Get Text    ${entry}
      
    #    # Extract title
    #    ${title_line}    Set Variable    ${entry_text.splitlines()[0]}
    #    Append To File    ${OutputFile}    ${title_line}\n
    #    Log To Console    ${title_line}    # Print the movie title

    #    # Extract date
    #    ${date_line}    Set Variable    ${entry_text.splitlines()[1]}
    #    Append To File    ${OutputFile}    ${date_line}\n
    #    Log To Console    ${date_line}    # Print the movie title



       # Extract metascore
       ${rating_line}    Set Variable    ${entry_text.splitlines()[-2]}
       Append To File    ${OutputFile}    ${rating_line}\n
       Log To Console    ${rating_line}    # Print the movie title
       END

    END






    
