from selenium import webdriver
from webdriver_manager.microsoft import EdgeChromiumDriverManager
from selenium.webdriver.edge.options import Options as EdgeOptions
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import time
from time import sleep
from selenium.webdriver.common.keys import Keys
import pandas as pd
from bs4 import BeautifulSoup
import numpy as np
import re
# Initialize EdgeOptions
options = EdgeOptions()

# Install Edge WebDriver using EdgeChromiumDriverManager and get the path
driver_path = EdgeChromiumDriverManager().install()

# Initialize the Edge WebDriver with options
driver = webdriver.Edge(options=options)




# Example usage: Open a website
driver.get("https://www.imdb.com/search/title/?num_votes=1000,5000&genres=horror")
# Example of accepting cookies if such functionality is needed

accept_button_xpath = '//button[@class="icb-btn sc-bcXHqe sc-dkrFOg sc-iBYQkv dcvrLS ddtuHe dRCGjd" and @data-testid="accept-button"]'
accept_button = driver.find_elements(By.XPATH, accept_button_xpath)
if accept_button:
    accept_button[0].click()

# Further actions with the driver can be performed here
driver.maximize_window()
driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

sleep(0.3)


for counter in range(1):
    entries=[]
    titles=[]
    ratings=[]
    metascores=[]
    html=driver.page_source
    soup=BeautifulSoup(html,'html.parser')
    data=soup.find("div", class_="ipc-page-grid__item ipc-page-grid__item--span-2")
    entries_contents = data.findAll('div', {'class': 'sc-5bc66c50-0 bZBaVw'})
  
    
    
    
    
    for entry_content in entries_contents:

        cell=entry_content.text
        entries.append(cell)
    
    for entry in entries:

        
         # Extracting Movie Title
        title_match = re.search(r'^(\d+)\. (.+?)(\d{4}).*?(\d{1,2}h \d{1,2}m)?(\d{2}).*?(\d+\.\d+)?', entry)
        if title_match:
            movie_title = title_match.groups()
            title = movie_title[1] if len(movie_title) > 1 else ""
            rating = movie_title[5] if len(movie_title) > 5 else ""
            
        else:
            continue   
        
        titles.append(title)
        ratings.append(rating)
        
                           
            
        
            
        # Extracting Metascore
        metascore_match = re.search(r'Rate([\d]+)', entry)
       
        if metascore_match:
            metascore = int(metascore_match.group(1))
        else:
            metascore = ""
        metascores.append(metascore)
        
    
        
    

    

   
    # Check if there's a "50 more" button
    more_button_xpath = '//button[contains(@class, "ipc-btn--single-padding") and contains(@class, "ipc-btn--center-align-content") and contains(@class, "ipc-see-more__button") and .//span[@class="ipc-btn__text"]/span[@class="ipc-see-more__text"][text()="50 more"]]'
    more_button = driver.find_elements(By.XPATH, more_button_xpath)

    if not more_button:
        break  # No more button, exit the loop

    # Scroll into view using JavaScript
    driver.execute_script("arguments[0].scrollIntoView();", more_button[0])

    # Click using JavaScript
    driver.execute_script("arguments[0].click();", more_button[0])

    time.sleep(2)  # Add a short delay to allow the new page to load
    counter += 1



df1=pd.DataFrame(titles, columns=['Title'])

df1['Rating']=ratings
df1['Metascore']=metascores

df1.to_excel (r'C:\Users\ikerasiotis\Desktop\imdb_dataframe.xlsx', index = False, header=True,sheet_name="2")
  
# Close the browser session
driver.quit()
